//
//  AddViewController.swift
//  iosApp
//
//  Created by Ina Saltovic on 6/2/19.
//  Copyright © 2019 Ina Saltovic. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        let name = nameField.text!
        let lastName = lastNameField.text!
        
        if(name == "" || lastName == ""){
            let alertController1 = UIAlertController(title: "Text fields not filled properly", message: "", preferredStyle: .alert)
            alertController1.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController1, animated: true, completion: nil)
        } else {
            let person = Person(context: PersistenceManager.context)
            person.firstName = name
            person.lastName = lastName
            
            PersistenceManager.saveContext()
            
            let alertController2 = UIAlertController(title: name + " " + lastName + " added to list.", message: "Congrats!", preferredStyle: .alert)
            alertController2.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController2, animated: true, completion: nil)
            
            nameField.text = ""
            lastNameField.text = ""
        }
    }
    
}
