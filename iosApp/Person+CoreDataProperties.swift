//
//  Person+CoreDataProperties.swift
//  iosApp
//
//  Created by Ina Saltovic on 6/2/19.
//  Copyright © 2019 Ina Saltovic. All rights reserved.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var firstName: String
    @NSManaged public var lastName: String

}
