//
//  ViewController.swift
//  iosApp
//
//  Created by Ina Saltovic on 6/2/19.
//  Copyright © 2019 Ina Saltovic. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        listPeople()
    }
    
    func listPeople(){
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let people = try PersistenceManager.context.fetch(request)
            self.people = people
            self.tableView.reloadData()
        } catch {
            print("Error loading data")
        }
    }


}

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = people[indexPath.row].firstName + String(" ") + people[indexPath.row].lastName
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            let person = people[indexPath.row]
            people.remove(at: indexPath.row)
            PersistenceManager.context.delete(person)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            PersistenceManager.saveContext()
        }
    }
}
